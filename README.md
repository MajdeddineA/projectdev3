![alt text](https://simplon.co/images/logo-simplon.png)
# Welcome :

## Getting Started
You can download the code with the assets file which contain the css and scss file.

## Installation
1. Clone the repo
git clone https://github.com/your_username_/Project-Name.git
2. Pull the code from the repository

## Usage

Use this space to show all the formations that Simplon of Grenoble presenting, and also you can contact with the administratots.

## Contributing
1. Fork the Project
2. Create your Feature Branch (git checkout -b feature/AmazingFeature)
3. Commit your Changes (git commit -m 'Add some AmazingFeature')
4. Push to the Branch (git push origin feature/AmazingFeature)
5. Open a Pull Request


## Built With
* Bootstrap
* HTML
* CSS
* SCSS
* Javascript

## Authors
* Estefania Vila
* Déborrah Brunier
* Majdeddine ALHAFEZ

## Please visit the web page:
[SIMPLON](http://simplon.co)




![alt text](https://simplon.co/storage/1148/responsive-images/haut_page_image1___medialibrary_original_1025_285.jpg)
